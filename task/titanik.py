import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    titles = ['Mr.', 'Mrs.', 'Miss.']
    results = []

    df['Title'] = df['Name'].str.extract('([A-Za-z]+)\.', expand=False)

    for title in titles:
        group = df[df['Title'] == title.strip('.')]
        missing_values = group['Age'].isnull().sum()
        median_age = round(group['Age'].dropna().median())
        results.append((title, missing_values, int(median_age)))

    return results


